VERSION := $(shell cat version.txt)

release:
	echo $(VERSION)	
	git tag -a v$(VERSION) -m "version $(VERSION)"
	git push origin v$(VERSION)
